#! /bin/bash
while getopts la:r:g:s: flag
do
    case "${flag}" in
        l) list=true;;
        a) add=${OPTARG};;
        r) remove=${OPTARG};;
        g) get=${OPTARG};;
        s) sub=${OPTARG};;
    esac
done
REDIS_POD=$(kubectl -n c4i get pod -l app=redis -o jsonpath="{.items[0].metadata.name}")
if [ $add ]
then
	kubectl -n c4i exec $REDIS_POD -- redis-cli SADD registered-users $add
fi
if [ $remove ]
then
	kubectl -n c4i exec $REDIS_POD -- redis-cli SREM registered-users $remove
fi
if [ $list ]
then
    kubectl -n c4i exec $REDIS_POD -- redis-cli SMEMBERS registered-users
fi
if [ $get ]
then
    echo
    echo "Current notebook:"
    kubectl -n c4i exec $REDIS_POD -- redis-cli GET notebook:$get
    echo
    echo "Subs:"
    kubectl -n c4i exec $REDIS_POD -- redis-cli SMEMBERS subs:$get
    echo
fi
if [ $sub ]
then
    while IFS= read -r key; do
        if [[ $(kubectl -n c4i exec $REDIS_POD -- redis-cli SISMEMBER $key $sub) = 1 ]]; then
            echo $key | tail -c +6
        fi
    done <<< "$(kubectl -n c4i exec $REDIS_POD -- redis-cli KEYS subs:*)"
fi
