import express from 'express';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import {
  RedisClientType,
  RedisDefaultModules,
  RedisFunctions,
  RedisModules,
  RedisScripts,
} from 'redis';
import { SWIRRL_API_ENDPOINTS, SWIRRL_API_BASEPATH } from '../config';

const swirrlRouter = (
  redisClient: RedisClientType<
    RedisDefaultModules & RedisModules,
    RedisFunctions,
    RedisScripts
  >
) => {
  const router = express.Router();

  router.use((req, res, next) => {
    console.log(`${req.url} SWIRRL API call, check email`);
    if (req.session.registered) {
      next();
    } else {
      console.log(`User is not registered`);
      res.sendStatus(401);
    }
  });

  type Notebook = {
    id: string;
    sessionId: string;
    poolId: string;
    serviceURL: string;
    userRequestedLibraries: Record<string, unknown>;
  };

  interface UserInfo {
    email: string;
    sub: string;
    iss: string;
  }

  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  router.post(SWIRRL_API_ENDPOINTS.notebook, async (req, res) => {
    console.log(
      'POST /notebook end point called. ' + JSON.stringify(req.params)
    );
    const headers = req.headers;
    headers.userInfo = req.session.token;
    try {
      const r = await axios.post<Notebook>(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.notebook}`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      );
      const notebook = r.data;
      console.log('NOTEBOOK ID: ' + JSON.stringify(notebook.id));
      if (req.session.token) {
        const userInfoBase64 = req.session.token.split('.');
        const userinfo = Buffer.from(userInfoBase64[1], 'base64').toString();
        const userinfoObj = JSON.parse(userinfo) as UserInfo;
        await redisClient.set(`notebook:${userinfoObj.email}`, notebook.id);
        await redisClient.set(
          `notebookURL:${notebook.id}`,
          notebook.serviceURL
        );
        await redisClient.set(`sessionId:${notebook.id}`, notebook.sessionId);
      }

      res.send(r.data);
    } catch (err) {
      const error = err as AxiosError;
      if (!error.response) {
        res.sendStatus(500);
      } else {
        res.status(error.response.status).send(error.response.data);
      }
    }
  });

  router.post(`${SWIRRL_API_ENDPOINTS.notebook}/:id/snapshot`, (req, res) => {
    console.log(`POST /notebook/${req.params.id}/snapshot end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .post(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.notebook}/${req.params.id}/snapshot`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.put(
    `${SWIRRL_API_ENDPOINTS.notebook}/:id/restorelibs/:activityId`,
    (req, res) => {
      console.log(
        `PUT /notebook/${req.params.id}/restorelibs/${req.params.activityId} end point called.`
      );
      const headers = req.headers;
      headers.userInfo = req.session.token;
      axios
        .put(
          `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.notebook}/${req.params.id}/restorelibs/${req.params.activityId}`,
          req.body,
          { headers } as unknown as AxiosRequestConfig
        )
        .then((r) => res.send(r.data))
        .catch((err: AxiosError) => {
          if (!err.response) {
            res.sendStatus(500);
          } else {
            res.status(err.response.status).send(err.response.data);
          }
        });
    }
  );

  router.put(`${SWIRRL_API_ENDPOINTS.notebook}/:id`, (req, res) => {
    console.log(`PUT /notebook/${req.params.id} end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .put(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.notebook}/${req.params.id}`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.get(`${SWIRRL_API_ENDPOINTS.notebook}/:id`, (req, res) => {
    console.log(`GET /notebook/${req.params.id} end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .get(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.notebook}/${req.params.id}`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  router.delete(`${SWIRRL_API_ENDPOINTS.notebook}/:id`, async (req, res) => {
    console.log(`DELETE /notebook/${req.params.id} end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    try {
      const r = await axios.delete(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.notebook}/${req.params.id}`,
        { headers } as unknown as AxiosRequestConfig
      );
      if (req.session.token) {
        const userInfoBase64 = req.session.token.split('.');
        const userinfo = Buffer.from(userInfoBase64[1], 'base64').toString();
        const userinfoObj = JSON.parse(userinfo) as UserInfo;
        try {
          const notebookId = await redisClient.get(
            `notebook:${userinfoObj.email}`
          );
          if (notebookId && notebookId === req.params.id) {
            await redisClient.del(`notebook:${userinfoObj.email}`);
            await redisClient.del(`notebookURL:${notebookId}`);
            await redisClient.del(`sessionId:${notebookId}`);
          }
        } catch (err) {
          console.error(err);
        }
      }
      res.send(r.data);
    } catch (err) {
      const error = err as AxiosError;
      if (!error.response) {
        res.sendStatus(500);
      } else {
        res.status(error.response.status).send(error.response.data);
      }
    }
  });

  router.delete(`${SWIRRL_API_ENDPOINTS.session}/:id`, (req, res) => {
    console.log(`DELETE /session/${req.params.id} end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .delete(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.session}/${req.params.id}`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.delete(
    `${SWIRRL_API_ENDPOINTS.workflow}/:id/deletelatest`,
    (req, res) => {
      console.log(
        `DELETE /session/${req.params.id}/deletelatest end point called.`
      );
      const headers = req.headers;
      headers.userInfo = req.session.token;
      axios
        .delete(
          `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.workflow}/${req.params.id}/deletelatest`,
          { headers } as unknown as AxiosRequestConfig
        )
        .then((r) => res.send(r.data))
        .catch((err: AxiosError) => {
          if (!err.response) {
            res.sendStatus(500);
          } else {
            res.status(err.response.status).send(err.response.data);
          }
        });
    }
  );

  router.delete(`${SWIRRL_API_ENDPOINTS.session}/unused`, (req, res) => {
    console.log(`DELETE /session/unused end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .delete(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.session}/unused`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.post(`${SWIRRL_API_ENDPOINTS.workflow}/:id/run`, (req, res) => {
    console.log(`POST /workflow/${req.params.id}/run/ end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    if (req.query.authenticated === 'true' && req.query.key) {
      headers.sessionID = `${Buffer.from(req.sessionID)
        .toString('base64')
        .replace(/=+$/, '')},${req.query.key as string}`;
    }
    axios
      .post(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.workflow}/${req.params.id}/run/`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.get(`${SWIRRL_API_ENDPOINTS.workflow}/:id/run/:jobid`, (req, res) => {
    console.log(
      `GET /workflow/${req.params.id}/run/${req.params.jobid} end point called.`
    );
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .get(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.workflow}/${req.params.id}/run/${req.params.jobid}`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.post(`${SWIRRL_API_ENDPOINTS.provenance}`, (req, res) => {
    console.log('POST /provenance end point called.');
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .post(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.provenance}`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.get(
    `${SWIRRL_API_ENDPOINTS.provenance}/session/:sessionId/activities`,
    (req, res) => {
      console.log(
        `GET /provenance/session/${req.params.sessionId}/activities end point called.`
      );
      const headers = req.headers;
      headers.userInfo = req.session.token;
      let queryString = '';
      for (const [key, value] of Object.entries(req.query)) {
        if (
          [
            'person',
            'poolId',
            'serviceId',
            'type',
            'includeRuns',
            'includeAdditionalInfo',
          ].indexOf(key) > -1 &&
          typeof value === 'string'
        ) {
          if (!queryString) {
            queryString += `?${key}=${value}`;
          } else {
            queryString += `&${key}=${value}`;
          }
        }
      }
      axios
        .get(
          `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.provenance}/session/${req.params.sessionId}/activities${queryString}`,
          { headers } as unknown as AxiosRequestConfig
        )
        .then((r) => res.send(r.data))
        .catch((err: AxiosError) => {
          if (!err.response) {
            res.sendStatus(500);
          } else {
            res.status(err.response.status).send(err.response.data);
          }
        });
    }
  );

  router.get(
    `${SWIRRL_API_ENDPOINTS.provenance}/activity/:activityId`,
    (req, res) => {
      console.log(
        `GET /provenance/activity/${req.params.activityId} end point called.`
      );
      const headers = req.headers;
      headers.userInfo = req.session.token;
      axios
        .get(
          `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.provenance}/activity/${req.params.activityId}`,
          { headers } as unknown as AxiosRequestConfig
        )
        .then((r) => res.send(r.data))
        .catch((err: AxiosError) => {
          if (!err.response) {
            res.sendStatus(500);
          } else {
            res.status(err.response.status).send(err.response.data);
          }
        });
    }
  );

  router.get(
    `${SWIRRL_API_ENDPOINTS.provenance}/service/:serviceId/sessionIds`,
    (req, res) => {
      console.log(
        `GET /provenance/service/${req.params.serviceId}/sessionIds end point called.`
      );
      const headers = req.headers;
      headers.userInfo = req.session.token;
      axios
        .get(
          `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.provenance}/service/${req.params.serviceId}/sessionIds`,
          { headers } as unknown as AxiosRequestConfig
        )
        .then((r) => res.send(r.data))
        .catch((err: AxiosError) => {
          if (!err.response) {
            res.sendStatus(500);
          } else {
            res.status(err.response.status).send(err.response.data);
          }
        });
    }
  );

  router.post(SWIRRL_API_ENDPOINTS.enlighten, (req, res) => {
    console.log(
      'POST /enlighten end point called. ' + JSON.stringify(req.params)
    );
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .post(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.enlighten}`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.get(`${SWIRRL_API_ENDPOINTS.enlighten}/:id`, (req, res) => {
    console.log(`GET /enlighten/${req.params.id} end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .get(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.enlighten}/${req.params.id}`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.delete(`${SWIRRL_API_ENDPOINTS.enlighten}/:id`, (req, res) => {
    console.log(`DELETE /enlighten/${req.params.id} end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .delete(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.enlighten}/${req.params.id}`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.get(`${SWIRRL_API_ENDPOINTS.github}/client_id`, (req, res) => {
    console.log(`GET /github/client_id end point called.`);
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .get(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.github}/client_id`,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  router.post(`${SWIRRL_API_ENDPOINTS.github}/access_token`, (req, res) => {
    console.log(
      'POST /github/access_token end point called. ' +
        JSON.stringify(req.params)
    );
    const headers = req.headers;
    headers.userInfo = req.session.token;
    axios
      .post(
        `http://swirrl-api.swirrl.svc.cluster.local${SWIRRL_API_BASEPATH}${SWIRRL_API_ENDPOINTS.github}/access_token`,
        req.body,
        { headers } as unknown as AxiosRequestConfig
      )
      .then((r) => res.send(r.data))
      .catch((err: AxiosError) => {
        if (!err.response) {
          res.sendStatus(500);
        } else {
          res.status(err.response.status).send(err.response.data);
        }
      });
  });

  return router;
};

export default swirrlRouter;
