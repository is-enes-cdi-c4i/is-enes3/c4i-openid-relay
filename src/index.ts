import { Issuer, GrantBody } from 'openid-client';

import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';

import session from 'express-session';
import RedisStore from 'connect-redis';
import redis from 'redis';

import { encrypt, decrypt } from './crypt';

import swirrlApiRouter from './routers/swirrlApi';

import {
  ISSUER,
  ENDPOINTS,
  C4IORIGIN,
  NOTEBOOKORIGIN,
  CLIENTID,
  CLIENTSECRET,
  SECRET,
  SWIRRL_API_BASEPATH,
} from './config';

declare module 'express-session' {
  interface SessionData {
    token: string;
    registered: boolean;
  }
}

const delay = (t: number): Promise<void> =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, t);
  });

const redirectURI = `${C4IORIGIN}/c4i-frontend/login`;

let issuer;
for (let i = 0; i < 300; i += 1) {
  try {
    issuer = await Issuer.discover(ISSUER);
  } catch (err) {
    console.error(err);
    await delay(60000);
    continue;
  }
  break;
}
if (!issuer) {
  throw new Error('Issuer.discover failed for 5 hours. Exiting...');
}

const client = new issuer.Client({
  client_id: CLIENTID,
  client_secret: CLIENTSECRET,
  response_types: ['code'],
});

const redisClient = redis.createClient({ url: 'redis://redis:80' });
await redisClient.connect();

const app = express();
const cookie = {
  maxAge: 2 * 60 * 60 * 1000, // 2 hours
  secure: C4IORIGIN.startsWith('https://'),
  sameSite: 'strict' as const,
};
app.set('trust proxy', 1);

app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    cookie,
    secret: SECRET,
    saveUninitialized: false,
    resave: false,
  })
);
app.use(helmet());
app.use(cors({ origin: [C4IORIGIN, NOTEBOOKORIGIN], credentials: true }));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  console.log(req.sessionID);
  console.log(req.session);
  req.query.key = req.headers.cookie
    ?.split('; ')
    .find((cookieHeader) => cookieHeader.split('=')[0] === 'connect.key')
    ?.split('=')[1];
  next();
});

app.use(`/relay${SWIRRL_API_BASEPATH}`, swirrlApiRouter(redisClient));

// eslint-disable-next-line @typescript-eslint/no-misused-promises
app.get(ENDPOINTS.end_session_endpoint, async (req, res) => {
  if (req.sessionID) {
    console.log(`Logging out session with id: ${req.sessionID}`);
    // Remove access token, proxyId, sessionId from Redis:
    await redisClient.del(`sess:${req.sessionID}`);
    // Remove cookie:
    req.session.destroy(() =>
      res
        .clearCookie('connect.sid', cookie)
        .clearCookie('connect.key', cookie)
        .redirect(client.endSessionUrl())
    );
  } else {
    res.sendStatus(401);
  }
});

app.get(ENDPOINTS.authorization_endpoint, (req, res) => {
  let queryURI = req.query.redirect_uri;
  if (
    typeof queryURI !== 'string' ||
    (queryURI.indexOf(C4IORIGIN) < 0 && queryURI.indexOf(NOTEBOOKORIGIN) < 0)
  ) {
    queryURI = redirectURI;
  }
  if (typeof req.query.state === 'string') {
    res.redirect(
      client.authorizationUrl({
        response_type: 'code',
        scope: 'openid email profile offline_access',
        state: req.query.state,
        redirect_uri: queryURI,
      })
    );
  } else {
    console.error('Invalid state provided');
    res.sendStatus(400);
  }
});

// eslint-disable-next-line @typescript-eslint/no-misused-promises
app.post(ENDPOINTS.token_endpoint, async (req, res) => {
  try {
    console.log('Requesting token...');
    console.log(req.body);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
    let queryURI = req.body.redirect_uri;
    if (
      typeof queryURI !== 'string' ||
      (queryURI.indexOf(C4IORIGIN) < 0 && queryURI.indexOf(NOTEBOOKORIGIN) < 0)
    ) {
      queryURI = redirectURI;
    }
    const tokenSet = await client.grant({
      ...req.body,
      redirect_uri: queryURI,
      grant_type: 'authorization_code',
    } as GrantBody);
    if (tokenSet.access_token && tokenSet.refresh_token) {
      req.session.token = tokenSet.access_token;
      await redisClient.sAdd('sessions', req.sessionID);
      const encryptedRefreshToken = encrypt(tokenSet.refresh_token);
      res.cookie('connect.key', encryptedRefreshToken.key, {
        ...cookie,
        httpOnly: true,
      });
      await redisClient.set(
        `refresh:${req.sessionID}`,
        encryptedRefreshToken.data
      );
      const userInfo = await client.userinfo(tokenSet);
      req.session.registered = false;
      const userList = await redisClient.sMembers('registered-users');
      for (const user of userList) {
        console.log(`User: ${user}`);
        if (
          userInfo.email &&
          user.toLowerCase() === userInfo.email.toLowerCase()
        ) {
          console.log(
            `User ${userInfo.email} is registered, adding sub to redis...`
          );
          await redisClient.sAdd(`subs:${userInfo.email}`, userInfo.sub);
          req.session.registered = true;
          break;
        }
      }
      userInfo.registered = req.session.registered;
      res.send(userInfo);
    } else {
      res.sendStatus(500);
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

// eslint-disable-next-line @typescript-eslint/no-misused-promises
app.get(ENDPOINTS.userinfo_endpoint, async (req, res) => {
  try {
    const encryptedRefreshToken = await redisClient.get(
      `refresh:${req.sessionID}`
    );
    if (encryptedRefreshToken && req.query.key) {
      const refreshToken = decrypt({
        data: encryptedRefreshToken,
        key: req.query.key as string,
      });
      const tokenSet = await client.grant({
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
      });
      const userInfo = await client.userinfo(tokenSet);
      userInfo.registered = req.session.registered;
      if (userInfo.email) {
        const notebookId = await redisClient.get(`notebook:${userInfo.email}`);
        userInfo.notebookId = notebookId;
        if (notebookId) {
          userInfo.notebookURL = await redisClient.get(
            `notebookURL:${notebookId}`
          );
          userInfo.sessionId = await redisClient.get(`sessionId:${notebookId}`);
        }
      }
      res.send(userInfo);
    } else {
      res.sendStatus(401);
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

app.listen(3000, () => console.log('App listening...'));
