import crypto from 'crypto';

export const ISSUER = 'https://esgf-login.ceda.ac.uk/realms/esgf';

const BASEPATH = '/protocol/openid-connect';

export const ENDPOINTS = {
  authorization_endpoint: `${BASEPATH}/auth`,
  jwks_uri: `${BASEPATH}/certs`,
  token_endpoint: `${BASEPATH}/token`,
  introspection_endpoint: `${BASEPATH}/token/introspect`,
  userinfo_endpoint: `${BASEPATH}/userinfo`,
  end_session_endpoint: `${BASEPATH}/logout`,
};

export const SWIRRL_API_BASEPATH = '/swirrl-api/v1.0';
export const SWIRRL_API_ENDPOINTS = {
  notebook: '/notebook',
  session: '/session',
  workflow: '/workflow',
  provenance: '/provenance',
  enlighten: '/enlighten',
  github: '/github',
};

export const C4IORIGIN = process.env.C4I_ORIGIN || '';
export const NOTEBOOKORIGIN = process.env.NOTEBOOK_ORIGIN || '';
export const CLIENTID = process.env.CLIENT_ID || '';
export const CLIENTSECRET = process.env.CLIENT_SECRET || '';
export const SECRET =
  process.env.SESSION_SECRET || crypto.randomBytes(32).toString('hex');

export const NAMESPACE = 'c4i';
