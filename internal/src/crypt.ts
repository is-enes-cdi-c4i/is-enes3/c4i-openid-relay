import crypto from 'crypto';

const algorithm = 'aes-256-cbc';

type EncryptedMessage = {
  data: string;
  key: string;
};

export const encrypt = (message: string): EncryptedMessage => {
  // generate 16 bytes of random data
  const initVector = crypto.randomBytes(16).toString('hex');

  // secret key generate 32 bytes of random data
  const securitykey = crypto.randomBytes(32).toString('hex');

  // the cipher function
  const cipher = crypto.createCipheriv(
    algorithm,
    Buffer.from(securitykey, 'hex'),
    Buffer.from(initVector, 'hex')
  );

  // encrypt the message
  // input encoding
  // output encoding
  let encryptedData = cipher.update(message, 'utf-8', 'hex');
  encryptedData += cipher.final('hex');
  return {
    data: encryptedData,
    key: `${initVector}.${securitykey}`,
  };
};

export const decrypt = (message: EncryptedMessage): string => {
  const initVector = message.key.split('.')[0];
  const securitykey = message.key.split('.')[1];

  // the decipher function
  const decipher = crypto.createDecipheriv(
    algorithm,
    Buffer.from(securitykey, 'hex'),
    Buffer.from(initVector, 'hex')
  );

  let decryptedData = decipher.update(message.data, 'hex', 'utf-8');

  decryptedData += decipher.final('utf8');

  return decryptedData;
};
