import { Issuer } from 'openid-client';

import express from 'express';

import redis from 'redis';
import { ISSUER, CLIENTID, CLIENTSECRET } from './config';

import { decrypt } from './crypt';

const delay = (t: number): Promise<void> =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, t);
  });

let issuer;
for (let i = 0; i < 300; i += 1) {
  try {
    issuer = await Issuer.discover(ISSUER);
  } catch (err) {
    console.error(err);
    await delay(60000);
    continue;
  }
  break;
}
if (!issuer) {
  throw new Error('Issuer.discover failed for 5 hours. Exiting...');
}

const client = new issuer.Client({
  client_id: CLIENTID,
  client_secret: CLIENTSECRET,
  response_types: ['code'],
});

const redisClient = redis.createClient({ url: 'redis://redis:80' });
await redisClient.connect();

const app = express();
// eslint-disable-next-line @typescript-eslint/no-misused-promises
app.get('/access_token/:sessionID', async (req, res) => {
  try {
    const b64sessionID =
      req.params.sessionID +
      '='.repeat((4 - (req.params.sessionID.length % 4)) % 4);
    const session = Buffer.from(b64sessionID, 'base64').toString();
    const encryptedRefreshToken = await redisClient.get(`refresh:${session}`);
    if (encryptedRefreshToken && req.query.key) {
      const refreshToken = decrypt({
        data: encryptedRefreshToken,
        key: req.query.key as string,
      });
      const tokenSet = await client.grant({
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
      });
      res.send(tokenSet.access_token);
    } else {
      res.sendStatus(401);
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});
app.listen(4000, () => console.log('App listening...'));
