export const ISSUER = 'https://esgf-login.ceda.ac.uk/realms/esgf';
export const CLIENTID = process.env.CLIENT_ID || '';
export const CLIENTSECRET = process.env.CLIENT_SECRET || '';
