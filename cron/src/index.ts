import * as k8s from '@kubernetes/client-node';
import redis from 'redis';

const NAMESPACE = 'swirrl';

const redisClient = redis.createClient({ url: 'redis://redis:80' });
await redisClient.connect();

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sCoreApi = kc.makeApiClient(k8s.CoreV1Api);

const hasPodOrSession = async (sessionID: string): Promise<boolean> => {
  const expressId = Buffer.from(sessionID)
    .toString('base64')
    .replace(/=+$/, '');
  const podResponse = await k8sCoreApi
    .listNamespacedPod(NAMESPACE)
    .catch((err) => console.error(err));
  if (podResponse) {
    const pods = podResponse.body.items;
    const hasPod = pods.some(
      (pod) =>
        pod.metadata &&
        pod.metadata.labels &&
        pod.metadata.labels.expressId === expressId
    );
    if (hasPod) return true;
  }
  const redisRes = await redisClient.get(`sess:${sessionID}`);
  if (redisRes) return true;
  return false;
};

const deleted: string[] = [];
const sessionIDs = await redisClient.sMembers('sessions');
for (const sessionID of sessionIDs) {
  if (!(await hasPodOrSession(sessionID))) {
    await redisClient.del(`refresh:${sessionID}`);
    await redisClient.sRem('sessions', sessionID);
    deleted.push(sessionID);
  }
}
console.log('Deleted refresh tokens for sessions:');
console.log(deleted);
process.exit();
