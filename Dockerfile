FROM node:18-bullseye-slim

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

# Install dependencies
COPY package.json ./package.json
COPY yarn.lock ./yarn.lock
COPY tsconfig.json ./tsconfig.json
RUN yarn --frozen-lockfile
# unfixed vulnerability in @kubernetes/client-node: https://www.npmjs.com/advisories/1091725
# RUN yarn audit

# Compile typescript
COPY index.ts ./index.ts
COPY src ./src
RUN yarn run tsc

# Remove dev dependencies
RUN rm -rf node_modules
RUN yarn --prod --frozen-lockfile

CMD [ "node", "--experimental-specifier-resolution=node", "index.js" ]
